<?php

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

class PdoqtTest extends \PHPUnit\Framework\TestCase {
    public function testVals () {
        $db = new \Pdoqt\Pdoqt(new \MockPDO);

        $rows = [
            ['a' => 1,  'b' => 2,  'c' => 3],
            ['a' => 11, 'b' => 12, 'c' => 13],
        ];
        list($vals, $params) = $db->vals($rows, ['a']);
        $this->assertEquals($vals, "(?),\n(?)", 'single column vals');
        $this->assertEquals($params, [1, 11], 'single column params');
        list($vals, $params) = $db->vals($rows, ['a', 'c']);
        $this->assertEquals($vals, "(?, ?),\n(?, ?)", 'two column vals');
        $this->assertEquals($params, [1, 3, 11, 13], 'two column params');
        list($vals, $params) = $db->vals($rows, ['a', 'b' => "'Bee'", 'c']);
        $this->assertEquals($vals, "(?, 'Bee', ?),\n(?, 'Bee', ?)", 'two column vals with constant');
        $this->assertEquals($params, [1, 3, 11, 13], 'two column params with constant');
    }
}

class MockPDO extends \PDO {
    public function __construct () {}
}
