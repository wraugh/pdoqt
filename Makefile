README.md: node_modules src/Pdoqt.php
	sed 1,2d src/Pdoqt.php > Pdoqt; \
		./node_modules/.bin/docco -l plain-markdown -o . -e .php Pdoqt > /dev/null; \
		awk '/```/&&++v%2{sub(/```/, "```php")}{print}' Pdoqt.html > README.md; \
		rm Pdoqt Pdoqt.html

docs: node_modules src/Pdoqt.php
	sed 1,2d src/Pdoqt.php > Pdoqt; \
		./node_modules/.bin/docco -l linear -e .php Pdoqt; \
		rm Pdoqt

.PHONY: test
test: vendor
	./vendor/bin/phpunit tests/PdoqtTest.php

node_modules:
	npm install

vendor:
	composer install
